﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrovistaDotNetTechTest.data
{

    public class DbContext : IDbContext
    {
        public IQueryable<Supplier> Suppliers
        {
            get
            {
                return new List<Supplier>
                {
                    new Supplier
                    {
                        SupplierId = 1,
                        Name = "Agchem Supplier 1",
                        LeadTime = 1
                    },
                    new Supplier
                    {
                        SupplierId = 2,
                        Name = "Agchem Supplier 2",
                        LeadTime = 2
                    },
                    new Supplier
                    {
                        SupplierId = 3,
                        Name = "Seed Supplier 2",
                        LeadTime = 1
                    },
                    new Supplier
                    {
                        SupplierId = 4,
                        Name = "Amenity Supplier 1",
                        LeadTime = 3
                    },
                    new Supplier
                    {
                        SupplierId = 5,
                        Name = "Amenity Supplier 2",
                        LeadTime = 6
                    }
                    ,
                    new Supplier
                    {
                        SupplierId = 6,
                        Name = "Seed Supplier 1",
                        LeadTime = 13
                    }
                }.AsQueryable();
            }
        }

        public IQueryable<Product> Products
        {
            get
            {
                return new List<Product>
                           {
                               new Product { ProductId = 1, Name = "VOLLEY (5 LITRE)", SupplierId = 1 },
                               new Product { ProductId = 2, Name = "HAMLET (5 LITRE)", SupplierId = 2 },
                               new Product { ProductId = 3, Name = "PHUSION (5 LITRE)", SupplierId = 4 },
                               new Product { ProductId = 4, Name = "MEASURING JUG 1LTR (1 EACH)", SupplierId = 1 },
                               new Product { ProductId = 5, Name = "ROUNDUP ULTIMATE (15 LITRE)", SupplierId = 1 },
                               new Product { ProductId = 6, Name = "K-OBIOL ULV6 (20 LITRE)", SupplierId = 2 },
                               new Product { ProductId = 7, Name = "FRELIZON (5 LITRE)", SupplierId = 1 },
                               new Product { ProductId = 8, Name = "VAMINOC G (10 KG)", SupplierId = 7 },
                               new Product { ProductId = 9, Name = "ACARAMIK (1 LITRE)", SupplierId = 5 },
                               new Product { ProductId = 10, Name = "SPECIAL GRASS MIX MASON (15 KG)", SupplierId = 6 },
                           }.AsQueryable();
            }
        }
    }
}
