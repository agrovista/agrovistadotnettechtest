# .NET Engineering recruitment test

Welcome to the .NET Core engineering test. This test has been designed to allow 
you to show us how you think quality code should be written.  We have not set a 
time limit, you should try to complete the project to a level which best demonstrates
your problem solving and programming ability. Please feel free to make any changes you
want to the solution, add classes, remove projects etc.

We are interested in seeing how you approach the task so please commit more
regularly than you normally would so we can see each step and **include
the .git folder in your submission**.

When complete please upload your solution and answers in a .zip to the shared
drive link provided to you by the recruiter.

## Programming Exercise

You have been tasked with creating a service that calculates the estimated 
delivery dates of customers' orders. 

An order consists of an order date and a collection of products that a 
customer has added to their shopping basket. 

Each of these products is supplied to Agrovista on demand through a number of 
3rd party suppliers.

As soon as an order is received by a supplier, the supplier will start 
processing the order. The supplier has an agreed lead time in which to 
process the order before delivering it to the depot.

Once the depot has received all products in an order it is 
delivered to the customer.

**Assumptions**:

1. Suppliers start processing an order on the same day that the order is 
	received. For example, a supplier with a lead time of one day, receiving
	an order today will send it to the depot tomorrow.

2. For the purposes of this exercise we are ignoring time i.e. if a 
	supplier has a lead time of 1 day then an order received any time on 
	Tuesday would arrive at the depot on the Wednesday.

3. Once all products for an order have arrived at the depot from the suppliers,
	they will be delivered to the customer on the same day.

### Part 1 

When the /api/deliveryDate endpoint is hit return the delivery date of that
order.

### Part 2

depot staff are getting complaints from customers expecting packages to
be delivered on the weekend. You find out that the depot is 
shut over the weekend. Packages received from a supplier on a weekend will 
be delivered the following Monday.

Modify the existing code to ensure that any orders received from a supplier
on the weekend are delivered on the following Monday.

### Part 3

It turns out suppliers don't work during the weekend as well, i.e. if an order 
is received on the Friday with a lead time of 2 days, the depot would receive and 
delivery on the Tuesday.

Modify the existing code to ensure that any orders that would have been 
processed during the weekend resume processing on Monday.

---

Parts 1 & 2 have already been completed albeit lacking in quality. Please
review the code, document the problems you find (see question 1), and refactor
into what you would consider quality code.

Once you have completed the refactoring, extend your solution to capture the 
requirements listed in part 3.

Please note, the provided DbContext is a stubbed class which provides test 
data. Please feel free to use this in your implementation and tests but do 
keep in mind that it would be switched for something like an EntityFramework 
DBContext backed by a real database in production.

While completing the exercise please answer the questions listed below. 
bullet point answers will be sufficient here. You can add the answers in this 
document.

### Part 4 - Database
Now the service is working as expected, the business has asked you to store the data in a database. The solution should
be implemented using visual studios localdb with the scripts saved in the 'savedscripts' folder for application into production.

The database should have the following tables:

	- Products
	- Suppliers

The data should replicate the stubbed data in the DbContext and therefore have table creation scripts and data insertion scripts.

Additional considerations are as follows:

	- A product cannot exist without a supplier
	- Each supplier and product should have a unique identifier and must not be duplicated in the tables
	- Supplier name and product name should be optimised for search

Finally theres a new requirement to generate delivery dates in the database. Therefore the controller logic needs to be
implemented as a stored procedure in the database. The stored procedure should take in the order date and return
the delivery date for the product ids in the CurrentProductIds table.

Logic to create the table and insert some values have been provided in the 'CurrentProductIds.sql' file. These
can be updated as required.

## Questions

Q1. What 'code smells' / anti-patterns did you find in the existing 
	implementation of part 1 & 2?


Q2. What best practices have you used while implementing your solution?


Q3. What further steps would you take to improve the solution given more time?
  

Q4. What's a technology that you're excited about and where do you see this 
    being applicable? (Your answer does not have to be related to this problem)


## Request and Response Examples

Please see examples for how to make requests and the expected response below.

### Request

The service is setup as a Web API and takes a request in the following format

~~~~ 
GET /api/DeliveryDate?ProductIds={product_id}&orderDate={order_date} 
~~~~

e.g.

~~~~ 
GET /api/DeliveryDate?ProductIds=1&orderDate=2018-01-29T00:00:00
GET /api/DeliveryDate?ProductIds=2&ProductIds=3&orderDate=2018-01-29T00:00:00 
~~~~

### Response

The response will be a JSON object with a date property set to the resulting 
delivery Date

~~~~ 
{
    "date" : "2018-01-30T00:00:00"
}
~~~~ 

## Acceptance Criteria

### Lead time added to delivery date  

**Given** an order contains a product from a supplier with a lead time of 1 day  
**And** the order is place on a Monday - 01/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Tuesday - 02/01/2018  

**Given** an order contains a product from a supplier with a lead time of 2 days  
**And** the order is place on a Monday - 01/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Wednesday - 03/01/2018  

### Supplier with longest lead time is used for calculation

**Given** an order contains a product from a supplier with a lead time of 1 day  
**And** the order also contains a product from a different supplier with a lead time of 2 days  
**And** the order is place on a Monday - 01/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Wednesday - 03/01/2018  

### Lead time is not counted over a weekend

**Given** an order contains a product from a supplier with a lead time of 1 day  
**And** the order is place on a Friday - 05/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Monday - 08/01/2018  

**Given** an order contains a product from a supplier with a lead time of 1 day  
**And** the order is place on a Saturday - 06/01/18  
**When** the delivery date is calculated  
**Then** the delivery date is Tuesday - 09/01/2018  

**Given** an order contains a product from a supplier with a lead time of 1 days  
**And** the order is place on a Sunday - 07/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Tuesday - 09/01/2018  

### Lead time over multiple weeks

**Given** an order contains a product from a supplier with a lead time of 6 days  
**And** the order is place on a Friday - 05/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Monday - 15/01/2018  

**Given** an order contains a product from a supplier with a lead time of 11 days  
**And** the order is place on a Friday - 05/01/2018  
**When** the delivery date is calculated  
**Then** the delivery date is Monday - 22/01/2018
