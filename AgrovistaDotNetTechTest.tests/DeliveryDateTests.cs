using AgrovistaDotNetTechTest.api.Controllers;
using System;
using Shouldly;
using Xunit;
using System.Collections.Generic;

namespace AgrovistaDotNetTechTest.tests
{
    public class DeliveryDateTests
    {
        [Fact]
        public void OneProductWithLeadTimeOfOneDay()
        {
            DeliveryDateController controller = new DeliveryDateController();
            var date = controller.Get(new List<int>() { 1 }, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(1));
        }

        [Fact]
        public void OneProductWithLeadTimeOfTwoDay()
        {
            DeliveryDateController controller = new DeliveryDateController();
            var date = controller.Get(new List<int>() { 2 }, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(2));
        }

        [Fact]
        public void OneProductWithLeadTimeOfThreeDay()
        {
            DeliveryDateController controller = new DeliveryDateController();
            var date = controller.Get(new List<int>() { 3 }, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(3));
        }

        [Fact]
        public void SaturdayHasExtraTwoDays()
        {
            DeliveryDateController controller = new DeliveryDateController();
            var date = controller.Get(new List<int>() { 1 }, new DateTime(2018, 1, 26));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(3));
        }

        [Fact]
        public void SundayHasExtraDay()
        {
            DeliveryDateController controller = new DeliveryDateController();
            var date = controller.Get(new List<int>() { 3 }, new DateTime(2018, 1, 25));
            date.Date.ShouldBe(new DateTime(2018, 1, 25).Date.AddDays(4));
        }
    }
}
