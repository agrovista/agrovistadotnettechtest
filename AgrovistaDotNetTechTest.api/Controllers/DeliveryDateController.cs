﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgrovistaDotNetTechTest.api.Models;
using AgrovistaDotNetTechTest.data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgrovistaDotNetTechTest.api.Controllers
{
    [Route("api/[controller]")]
    public class DeliveryDateController : Controller
    {
        public DateTime _mlt;

        [HttpGet]
        public DeliveryDate Get(List<int> productIds, DateTime orderDate)
        {
            _mlt = orderDate; // max lead time
            foreach (var ID in productIds)
            {
                DbContext dbContext = new DbContext();
                var s = dbContext.Products.Single(x => x.ProductId == ID).SupplierId;
                var lt = dbContext.Suppliers.Single(x => x.SupplierId == s).LeadTime;
                if (orderDate.AddDays(lt) > _mlt)
                    _mlt = orderDate.AddDays(lt);
            }
            if (_mlt.DayOfWeek == DayOfWeek.Saturday)
            {
                return new DeliveryDate { Date = _mlt.AddDays(2) };
            }
            else if (_mlt.DayOfWeek == DayOfWeek.Sunday) return new DeliveryDate { Date = _mlt.AddDays(1) };
            else return new DeliveryDate { Date = _mlt };
        }
    }
}