﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgrovistaDotNetTechTest.api.Models
{
    public class DeliveryDate
    {
        public DateTime Date { get; set; }
    }
}
